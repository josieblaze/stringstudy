'''
This file I am creating to study lists

Created on Dec 13, 2014

@author: Josephine
'''
import sys
list1 = [1,2,3,4,5]

print type(list1)

print "list1 STARTS AS %s" % list1

# now I will reverse the list

count = len(list1)

print "Count is", count

temp_list = list()

while count > 0:
    print list1[count-1]
    temp_list.append(list1[count-1])
    count -= 1
    
print "setting list1 from the temp_list"
list1 = temp_list

print "list1 is now %s" % list1


